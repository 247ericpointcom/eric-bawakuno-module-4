import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter/src/widgets/scroll_view.dart';
import 'package:flutter/src/material/material.dart';
import 'package:shopper/signup_screen.dart';
import 'package:shopper/welcome_screen.dart';
import 'package:shopper/constants.dart';
import 'package:shopper/login_screen.dart';
import 'package:shopper/dashboard.dart';
import 'package:shopper/edit_profile.dart';
import 'package:flutter/services.dart';
import '../../constants.dart';

class Dashboard extends StatefulWidget{
  @override
  _DashboardState createState() => _DashboardState();
}
class _DashboardState extends State<Dashboard>{

  Material MyItems(IconData icon, String heading, int color){
    return Material(
        color: Colors.white,
        elevation: 14.0,
      shadowColor: Color(0x80196F3),
      borderRadius: BorderRadius.circular(24),
      child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                child: Text(heading,
                style: TextStyle(
                color: new Color(color),
                  fontSize: 20.0,
                      ),

                    ),
                  ),
                 Material(
                   color: new Color(color),
                  borderRadius: BorderRadius.circular(24.0),
                  child: Padding(
                  padding: const EdgeInsets.all(16.0),
                    child: Icon(icon,
                     color: Colors.white,
                      size: 30.0,
              ),

             ),
            ),
              ],
            )
          ],
            ),
          ),
      ),
    );
  }
  @override
  Widget build(BuildContext context){
      return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          title: Text('Shop Smartly',
            style: TextStyle(
              color: Colors.amber[600],
            ),
          ),
          elevation: 14.0,
          systemOverlayStyle: SystemUiOverlayStyle.light,
          backgroundColor: kBackgroundColor,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(Icons.arrow_back_ios),
            iconSize: 20,
            color: kTextColor,
          ),
        ),
        body:StaggeredGridView.count(
            crossAxisCount: 2,
            crossAxisSpacing: 12.0,
            mainAxisSpacing: 12.0,
            padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          children: <Widget>[
            MyItems(Icons.graphic_eq,"Total Product",0xffed622b),
            MyItems(Icons.graphic_eq,"Clothes",0xff26cb3c),
            MyItems(Icons.graphic_eq,"Groceries",0xffff3266),
            MyItems(Icons.graphic_eq,"Mobile Devices",0xff3399fe),
            MyItems(Icons.graphic_eq,"Cars",0xfff4c83f),
            MyItems(Icons.graphic_eq,"Plane Tickets",0xff622F74),
            MyItems(Icons.graphic_eq,"Food",0xff7297ff),
            MyItems(Icons.graphic_eq,"Rides",0xffed622b),
      ],
          staggeredTiles: [
            StaggeredTile.extent(2, 130.0),
            StaggeredTile.extent(1, 150.0),
            StaggeredTile.extent(1, 150.0),
            StaggeredTile.extent(1, 150.0),
            StaggeredTile.extent(1, 150.0),
            StaggeredTile.extent(1, 150.0),
            StaggeredTile.extent(1, 150.0),
            StaggeredTile.extent(2, 240.0),
            StaggeredTile.extent(2, 120.0),
          ],
        ),
          floatingActionButton: FloatingActionButton(
            onPressed: (){
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SettingsUI()));
            },
            child: Icon(Icons.navigation),
            backgroundColor: Colors.green,
          ), // This trailing comma makes auto-formatting nicer for build methods.

      );
  }
}
